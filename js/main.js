// Mostrar en pantallas
function writeScreen(value){

    //getElementById señala una "Id" del html
    //innerHTML captura el contenido de lo que hay dentro de la etiqueta html con esa id <p> </p>
    var operando = document.getElementById("entryNumber").innerHTML;

    alert(operando.indexOf("."));

    if(operando == ''){
        document.getElementById("entryNumber").innerHTML = value;
    }
    else if(operando == '0' && value != '.'){ // "Y Si" value es igual a "0" y distinto a "." se corta el flujo con "return false"
        return false; //Solo permite un cero al inicio 


    }else{
        if(value == '/' || value == '*' || value == '-' || value == '+'){
            operando += value; //concadena el numero anterior con el siguiente numero o simbolo
            document.getElementById("exitNumber").innerHTML = operando; //mostrara los numeros y signos de operaciones en "exitNumber"
            document.getElementById("entryNumber").innerHTML = ""; //Vaciara la pantalla "entryNumber"
       //Si el valor es igual a "punto"
        }else if(value == '.'){
            //Si la pantalla tiene un punto, entonces:
            if (operando.indexOf('.') == -1) {
                operando += '';
            //Si no tiene un punto, entonces:
            }else {
                operando += value;
            }
        }else {
            operando += value; //concadena el numero anterior con el siguiente numero o simbolo
            document.getElementById("entryNumber").innerHTML = operando; //mostrara los numeros en la pantalla "entryNumber"
        }
    }
}

//Operaciones
function result(){ //Juntara "exitNumber" con "entryNumber y los mostrara en "exitNumber"
    var operacion = document.getElementById("exitNumber").innerHTML + document.getElementById("entryNumber").innerHTML;
    
    document.getElementById("entryNumber").innerHTML = eval(operacion); //eval(...); ejecuta la opracion dentro de "operacion"
    document.getElementById("exitNumber").innerHTML = ""; //Vaciar la pantalla "exitNumber"
}

//Borrar
//CE
function erase(){ //Vacia la pantalla inferior "entryNumber"
    var operacion = document.getElementById("entryNumber").innerHTML = "";
}
//C
function eraseAll(){ //Vaciar ambas pantallas
    var operacion = document.getElementById("entryNumber").innerHTML = "";
    var operacion = document.getElementById("exitNumber").innerHTML = "";
}
//← Borrar Ultimo
function retroceso() { //Borrar el eltimo numero de la fila
    var retro = document.getElementById("entryNumber").innerHTML
    document.getElementById("entryNumber").innerHTML = retro.slice(0, -1); 
    //Borra el ultimo numero de la fila usando "slice" que recarga los numeros mostrado en pantalla,
    //pero al añadir "-1" no se carga la ultima cifra, así parece que se borro.
}


//function writeScreen(){
//    var operando = document.getElementById("entryNumber").innerHTML;
//    if (operando == "0" || operando == ''){
//        document.getElementById("exitNumber").innerHTML = (-1);
//    }
    
//}

// if(inpResult.value.indexOf(".") == -1)
// inpResult.value += ".";
// shouldEmptyResult = false;